import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {

  mensaje = '';

  showBtn = 'false';

  constructor(private router: Router) {
    this.mensaje = 'entraste a la pag de Error'

    this.mostrarBoton();
  }

  ngOnInit(): void {
    console.log(this.mensaje);
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  mostrarBoton(){
    (async () => {
      // Do something before delay
      console.log('before delay')

      await this.delay(1500);

      this.showBtn = 'true';

      // Do something after
      console.log('after delay')
    })();
  }

  irAlincio(){
    this.router.navigate(['']);
  }

}
