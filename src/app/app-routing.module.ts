import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components
import { ErrorPageComponent } from './error-page/error-page.component';
import { HomeComponent } from './home/home.component';
import { DatosBasicosComponent } from './datos-basicos/datos-basicos.component';
import { EnvioComponent } from './envio/envio.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'datos-basicos', component: DatosBasicosComponent },
  { path: 'envio', component: EnvioComponent },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
